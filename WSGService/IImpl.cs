﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WSGService {
    [ServiceContract]
    public interface IImpl {
        [OperationContract]
        byte[] GetContent(int id, string type);
    }
}